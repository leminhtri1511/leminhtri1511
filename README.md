[![MasterHead](https://i.imgur.com/DCoUjqb.gif)](https://github.com/leminhtri1511)

<p align="left"> <img src="https://komarev.com/ghpvc/?username=leminhtri1511&label=Profile%20views&color=0e75b6&style=flat" alt="leminhtri1511" /> </p>
<h1 align="center">Hi 👋, I'm Le Minh Tri</h1>
<!-- <h3 align="center">A passionate frontend developer</h3> -->

<!-- <img align="right" alt="Coding" width="450" src="https://www.existus.com/assets/images/image-sw-development.gif"> -->


- 🏨 I’m currently study at <a href="https://husc.edu.vn/en/viewpage.php?page_id=1">Hue University of Sciences</a>
- 📝 I’m currently working on **HTMl/CSS, JS** and more!
- 📫 How to reach me: **trile20021511@gmail.com**

---

### :zap: GitHub Stats

<table>
<tr>
  <td width="48%">
    <img src="https://github-readme-stats.vercel.app/api/top-langs/?username=leminhtri1511&layout=compact" />
    <img src="https://github-readme-stats.vercel.app/api?username=leminhtri1511&hide=contribs,issues" />
  </td>
  <td width="45%">
<h3 align="center">Connect with me:</h3>
<p align="center">
<a href="https://fb.com/https://www.facebook.com/treesloli" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/facebook.svg" alt="https://www.facebook.com/treesloli" height="30" width="40" /></a>
<a href="https://instagram.com/https://www.instagram.com/treesloli/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="https://www.instagram.com/treesloli/" height="30" width="40" /></a>
<a href="https://discord.gg/https://discord.gg/PJcT74Wd" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/discord.svg" alt="https://discord.gg/PJcT74Wd" height="30" width="40" /></a>
</p>

<h3 align="center">Languages and Tools:</h3>
  <p align="center">
  <img alt="Visual Studio Code"  src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" width="40px" height="40px"/>
    <a href="https://www.microsoft.com/en-us/sql-server" target="_blank" rel="noreferrer"> <img src="https://www.svgrepo.com/show/303229/microsoft-sql-server-logo.svg" alt="mssql" width="40" height="40"/> </a> 
<p align="center"> 
  <a href="https://www.cprogramming.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a> 
  <a href="https://www.w3schools.com/cpp/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/> </a> 
  <a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/> </a> 
    </p>
  </td>
</tr>
<table>

<!-- <p><img align="left" src="https://github-readme-stats.vercel.app/api/top-langs?username=leminhtri1511&show_icons=true&locale=en&layout=compact" alt="leminhtri1511" /></p>

<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=leminhtri1511&show_icons=true&locale=en" alt="leminhtri1511" /></p>

<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=leminhtri1511&" alt="leminhtri1511" /></p> -->

